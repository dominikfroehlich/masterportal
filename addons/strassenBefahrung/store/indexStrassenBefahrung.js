import GenericTool from "../../../src/modules/tools/indexTools";
import composeModules from "../../../src/app-store/utils/composeModules";
import getters from "./gettersStrassenBefahrung";
import mutations from "./mutationsStrassenBefahrung";
import state from "./stateStrassenBefahrung";

export default composeModules([GenericTool, {
    namespaced: true,
    state: {...state},
    mutations,
    getters
}]);